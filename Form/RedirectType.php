<?php

namespace TMWK\RedirectBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RedirectType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('url_old', null, array('label' => 'URL Anterior', 'attr' => array()))
            ->add('url_new', null, array('label' => 'URL Nueva', 'attr' => array()))
            ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'TMWK\RedirectBundle\Entity\Redirect'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'redirect';
    }


}
