Instrucciones
===============

1. Para instalar ejecutar el siguiente comando: 
  
        composer require tmwk/redireccionador-de-url-symfony2
    
2. Agregar nuestro vendor al app/AppKernel.php de la siguiente manera:

        $bundles = array(
            ...
            new TMWK\RedirectBundle\TMWKRedirectBundle(),
            ...
        );

3. Cargar nuestro archivo de rutas en app/config/routing.yml\
    Agrega el siguiente codigo al final del archivo, esto es importante ya que
    si no esta al final hará que fallen las demás rutas.
      
        app_redirect:
            resource: "@TMWKRedirectBundle/Resources/config/routing.yml"
            
4. Cargar nuestro archivo de rutas en app/config/config.yml\
    Agrega el siguiente codigo.
      
        twig:
            exception_controller:  app.exception_controller:showAction        
    
5.  Para agregar al menu de la administración editar el archivo: \
    app/Resources/views/backend/Templates/sliderbar.html.twig y agregar el sigueinte codigo:
           
        <ul class="page-sidebar-menu page-sidebar-menu-light" data-keep-expanded="false" data-auto-scroll="true"  data-slide-speed="200">
        ...
            <li class="{% if currentPath starts with path("admin_redirect") %}active{% endif %}">
                <a href="{{ path("admin_redirect") }}">
                    <i class="icon-user"></i>
                    <span class="title">Redirecciones</span>
                    <span class="selected"></span>
                </a>
            </li>
        ...
        </ul>