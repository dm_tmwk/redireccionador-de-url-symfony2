<?php
/**
 * Created by PhpStorm.
 * User: mfigueroa
 * Date: 30/10/2018
 * Time: 10:32
 */

namespace TMWK\RedirectBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints as DoctrineAssert;

/**
 * @ORM\Entity
 * @ORM\Table(name="redirect")
 */
class Redirect
{
    /**
    * @ORM\Id
    * @ORM\Column(type="integer")
    * @ORM\GeneratedValue(strategy="AUTO")
    */
    protected $id;

    /**
    * @var string $url_old
    *
    * @ORM\Column(type="string", nullable=true)
    */
    protected $url_old;

    /**
    * @var string $url_new
    *
    * @ORM\Column(type="string", nullable=true)
    */
    protected $url_new;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set url_old
     *
     * @param string $urlOld
     * @return Redirect
     */
    public function setUrlOld($urlOld)
    {
        $this->url_old = $urlOld;

        return $this;
    }

    /**
     * Get url_old
     *
     * @return string 
     */
    public function getUrlOld()
    {
        return $this->url_old;
    }

    /**
     * Set url_new
     *
     * @param string $urlNew
     * @return Redirect
     */
    public function setUrlNew($urlNew)
    {
        $this->url_new = $urlNew;

        return $this;
    }

    /**
     * Get url_new
     *
     * @return string 
     */
    public function getUrlNew()
    {
        return $this->url_new;
    }
}
