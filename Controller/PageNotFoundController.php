<?php

namespace TMWK\RedirectBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Bundle\TwigBundle\Controller\ExceptionController;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class PageNotFoundController extends ExceptionController
{
    private $container;

    public function __construct(\Twig_Environment $twig, $debug, ContainerInterface $container)
    {
        parent::__construct($twig, $debug);
        $this->container = $container;
    }

    public function showAction(\Symfony\Component\HttpFoundation\Request $request, \Symfony\Component\HttpKernel\Exception\FlattenException $exception, \Symfony\Component\HttpKernel\Log\DebugLoggerInterface $logger = null, $_format = 'html')
    {
        $em          = $this->container->get('doctrine.orm.entity_manager');
        $current_url = $request->server->get('REQUEST_URI');

        $redirect = $em->getRepository('TMWKRedirectBundle:Redirect')->findOneBy(array('url_old' => $current_url));

        if ($redirect) {
            return new RedirectResponse($redirect->getUrlNew(), 301);
        } else {
            return parent::showAction($request, $exception, $logger, $_format);
//            throw new NotFoundHttpException('No route found.');
        }

    }
}