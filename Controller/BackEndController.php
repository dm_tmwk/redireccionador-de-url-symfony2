<?php

namespace TMWK\RedirectBundle\Controller;

use TMWK\RedirectBundle\Entity\Redirect;
use TMWK\RedirectBundle\Form\RedirectType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class BackEndController
 * @package RedirectBundle\Controller
 * @Route("/admin/redirect")
 */
class BackEndController extends Controller
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/", name="admin_redirect")
     */
    public function indexAction()
    {
        return $this->render('@TMWKRedirect/redirect/index.html.twig', array(

        ));
    }

    /**
     * @param Request $request
     *
     * @Route("/new", name="admin_redirect_new")
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = new Redirect();
        $form   = $this->createForm(new RedirectType(), $entity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirectToRoute('admin_redirect');
        }

        return $this->render('@TMWKRedirect/redirect/new.html.twig', array(
            'entity'  => $entity,
            'form'    => $form->createView()
        ));
    }

    /**
     * @param Request  $request
     * @param Redirect $redirect
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @Route("/{redirect}/edit", name="admin_redirect_edit")
     */
    public function editAction(Request $request, Redirect $redirect)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $redirect;
        $form   = $this->createForm(new RedirectType(), $entity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirectToRoute('admin_redirect_edit', array('redirect' => $redirect->getId()));
        }

        return $this->render('@TMWKRedirect/redirect/edit.html.twig', array(
            'entity'  => $entity,
            'form'    => $form->createView()
        ));
    }

    /**
     * @param Redirect $redirect
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @Route("/{redirect}/delete", name="admin_redirect_delete")
     */
    public function deleteAction(Redirect $redirect)
    {
        $em = $this->getDoctrine()->getManager();
        if ($redirect) {
            $em->remove($redirect);
            $em->flush();
        }

        return $this->redirectToRoute('', array());
    }

    /**
     * @Route("/json", name="admin_redirect_json")
     */
    public function jsonAction()
    {
        $em       = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('TMWKRedirectBundle:Redirect')->findAll();
        $array    = array();

        foreach ($entities as $val) {
            $options = '';
            $options .= $this->renderView(":backend/Templates:link_json.html.twig", array(
                "url"   => $this->generateUrl('admin_redirect_edit', array('redirect' => $val->getId())),
                "class" => "iframe",
                "icon"  => "icon-pencil",
                "text"  => "Editar"
            ));

            $options .= $this->renderView(":backend/Templates:link_json.html.twig", array(
                "url"         => $this->generateUrl('admin_redirect_delete', array('redirect' => $val->getId())),
                "class"       => "del-ajax",
                "icon"        => "icon-trash",
                "text"        => "Eliminar",
                "data_reload" => "reload_admin_redirect"
            ));

            $array[] = array(
                $val->getId(),
                $val->getUrlOld(),
                $val->getUrlNew(),
                $this->renderView(":backend/Templates:dropdown.html.twig", array(
                    "options" => $options
                ))
            );
        }

        $response = new Response();
        $response->setContent(json_encode(array(
            'aaData' => $array,
        )));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }
}
